package sega.gamal.tutorialapp.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sega.gamal.tutorialapp.model.ContentEnum;

/**
 * Created by godsun21 on 17/05/17.
 */

public class MainPagerAdapter extends PagerAdapter {

    private Context context;

    public MainPagerAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return ContentEnum.values().length; // return number model
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ContentEnum contentEnum = ContentEnum.values()[position];
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) layoutInflater.inflate(contentEnum.getResLayout(), container, false);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }
}
