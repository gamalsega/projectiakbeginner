package sega.gamal.tutorialapp.model;

import sega.gamal.tutorialapp.R;

/**
 * Created by godsun21 on 16/05/17.
 */

public enum ContentEnum {
    MAIN("Main Tutor", R.layout.view_main_tutor),
    TUTOR01("Tutorial 01", R.layout.view_tutor_01),
    TUTOR02("Tutorial 02", R.layout.view_tutor_02),
    TUTOR03("Tutorial 03", R.layout.view_tutor_03),
    TUTOR04("Tutorial 04", R.layout.view_tutor_04),
    TUTOR05("Tutorial 05", R.layout.view_tutor_05);

    private String header, content;
    private int resLayout;

    ContentEnum(String content, int resLayout) {
        this.content = content;
        this.resLayout = resLayout;
    }

    public String getContent() {
        return content;
    }

    public int getResLayout() {
        return resLayout;
    }
}
