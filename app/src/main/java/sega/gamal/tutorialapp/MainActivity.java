package sega.gamal.tutorialapp;


import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import sega.gamal.tutorialapp.adapter.MainPagerAdapter;
import sega.gamal.tutorialapp.fragment.AboutDialogFragment;


public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener, View.OnClickListener {

    private ViewPager viewPager;
    private ImageButton imgbutton_next, imgbutton_prev;
    private TextView textView;
    private MainPagerAdapter adapter;
    private int dotCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.pager_introduction);
        imgbutton_next = (ImageButton) findViewById(R.id.ibtn_next);
        imgbutton_next.setOnClickListener(this);
        imgbutton_prev = (ImageButton) findViewById(R.id.ibtn_prev);
        imgbutton_prev.setOnClickListener(this);
        adapter = new MainPagerAdapter(this);

        dotCount = adapter.getCount(); // hitung nilai item di adapter

        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        if (viewPager.getCurrentItem() == 0){
            imgbutton_prev.setVisibility(View.GONE);
        }else {
            imgbutton_prev.setVisibility(View.VISIBLE);
        }
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ibtn_next:
                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
                break;
            case R.id.ibtn_prev:
                viewPager.setCurrentItem(viewPager.getCurrentItem() - 1);
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0){ // jika posisi nya ke home maka tombol prev hilang
            imgbutton_prev.setVisibility(View.GONE);
        }else {
            imgbutton_prev.setVisibility(View.VISIBLE);
        }if (position + 1 == dotCount){ // jika posisi content nya di akhir maka tombol next hilang
            imgbutton_next.setVisibility(View.GONE);
        }else {
            imgbutton_next.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mn_about:
                AboutDialogFragment dialogFragment = new AboutDialogFragment();
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(dialogFragment, "about dialog");
                transaction.commit();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
